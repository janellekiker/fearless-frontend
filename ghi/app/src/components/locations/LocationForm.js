import React, { useEffect, useState } from 'react';

const LocationForm = () => {
    const [states, setStates] = useState([])
    const [formData, setFormData] = useState({
        name:"",
        room_count:0,
        city:"",
        state:""
    })

    // const [name, setName] = useState("");
    // const [roomCount, setRoomCount] = useState(0);
    // const [city, setCity] = useState("");
    // const [state, setState] = useState("");

    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    // }

    // const handleRoomCountChange = (event) => {
    //     const value = event.target.value;
    //     setRoomCount(value);
    // }

    // const handleCityChange = (event) => {
    //     const value = event.target.value;
    //     setCity(value);
    // }

    // const handleStateChange = (event) => {
    //     const value = event.target.value;
    //     setState(value);
    // }

    const handleSubmit = async (event) =>{
        event.preventDefault();

        // // create an empty JSON object
        // const data = {};

        // data.room_count = roomCount;
        // data.name = name;
        // data.city = city;
        // data.state = state;

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name:"",
                room_count:0,
                city:"",
                state:""
            })
        }
    }
        const handleInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value
            })
        }

    // LOAD STATE OPTIONS IN DROPDOWN
    const fetchData = async () => {
        // MAKE AN API CALL TO THE LIST OF STATES
        const stateUrl = "http://localhost:8000/api/states/";
        // FETCH THE DATA AT THE LIST STATES URL
        const stateResponse = await fetch(stateUrl);
        // IF RESPONSE IS OKAY
        if (stateResponse.ok) {
            // CHANGE THE RESPONSE TO A JSON FORMATTED DATA RESPONSE
            // ADD AWAIT SO THAT IT DOESN'T RETURN THE PROMISE BUT INSTEAD THE
            // VALUE OF THAT PROMISE WILL TURN INTO AFTER AWAIT
            const stateData = await stateResponse.json();
            setStates(stateData.states)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

        return (
            <div>
                <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input value={formData.name} onChange={handleInput} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.room_count} onChange={handleInput} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
                        <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.city} onChange={handleInput} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                        <select value={formData.state} onChange={handleInput} required name="state" id="state" className="form-select">
                            <option value="">Choose a state</option>
                            {states.map((state) => {
                                return (
                                    <option key={Object.values(state)} value={Object.values(state)}>
                                        {Object.keys(state)}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        )
        }

export default LocationForm
