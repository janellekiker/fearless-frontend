import React, { useEffect, useState } from 'react'


const PresentationForm = () => {
    const [conferences, setConferences] = useState([]);
    const [formData, setFormData] = useState({
        presenter_name:"",
        presenter_email:"",
        company_name:"",
        title:"",
        synopsis:"",
        conference:"",
    })

    // const [name, setName] = useState("");
    // const [email, setEmail] = useState("");
    // const [company, setCompany] = useState("");
    // const [title, setTitle] = useState("");
    // const [synopsis, setSynopsis] = useState("");
    // const [conference, setConference] = useState("");

    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    //   }

    // const handleEmailChange = (event) => {
    //     const value = event.target.value;
    //     setEmail(value);
    //   }

    // const handleCompanyChange = (event) => {
    //     const value = event.target.value;
    //     setCompany(value);
    //   }

    // const handleTitleChange = (event) => {
    //     const value = event.target.value;
    //     setTitle(value);
    //   }

    // const handleSynoposisChange = (event) => {
    //     const value = event.target.value;
    //     setSynopsis(value);
    //   }

    // const handleConferenceChange = (event) => {
    //     const value = event.target.value;
    //     setConference(value);
    //   }

    const handleSubmit = async (event) =>{
        event.preventDefault();

        // create an empty JSON object
        // const data = {};

        // data.presenter_name = name;
        // data.company_name = company;
        // data.presenter_email = email;
        // data.title = title;
        // data.synopsis = synopsis;
        // data.conference = conference;

        const presentationUrl = `http://localhost:8000${formData.conference}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                presenter_name:"",
                presenter_email:"",
                company_name:"",
                title:"",
                synopsis:"",
                conference:"",
            })
        }
    }

        const handleInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value
            })
        }


    // LOAD AVAILABLE CONFERENCES
    const fetchData = async () => {
        // MAKE AN API CALL TO THE LIST OF CONFERENCE
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        // FETCH THE DATA AT THE LIST CONFERENCES URL
        const conferenceResponse = await fetch(conferenceUrl);
        // IF RESPONSE IS OKAY
        if (conferenceResponse.ok) {
            // CHANGE THE RESPONSE TO A JSON FORMATTED DATA RESPONSE
            // ADD AWAIT SO THAT IT DOESN'T RETURN THE PROMISE BUT INSTEAD THE
            // VALUE OF THAT PROMISE WILL TURN INTO AFTER AWAIT
            const conferenceData = await conferenceResponse.json()
            setConferences(conferenceData.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div>
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input value={formData.presenter_name} onChange={handleInput} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.presenter_email} onChange={handleInput} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.company_name} onChange={handleInput} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.title} onChange={handleInput} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea value={formData.synopsis} onChange={handleInput} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                </div>
                <div className="mb-3">
                    <select value={formData.conference} onChange={handleInput} required name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map((conference) => {
                                return (
                                    <option key={conference.href} value={conference.href}>
                                        {conference.name}
                                    </option>
                                );
                            })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    )
}

export default PresentationForm
