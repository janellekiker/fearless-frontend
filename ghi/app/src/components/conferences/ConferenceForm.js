import React, { useEffect, useState } from 'react'

const ConferenceForm = () => {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        name:"",
        starts:"",
        ends:"",
        description:"",
        max_presentations:0,
        max_attendees:0,
        location:"",
    })

    // const [name, setName] = useState("");
    // const [starts, setStarts] = useState();
    // const [ends, setEnds] = useState();
    // const [description, setDescription] = useState("");
    // const [maxPresentations, setMaxPresentations] = useState(0);
    // const [maxAttendees, setMaxAttendees] = useState(0);
    // const [location, setLocation] = useState("")

    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    // }

    // const handleStartsChange = (event) => {
    //     const value = event.target.value;
    //     setStarts(value);
    // }

    // const handleEndsChange = (event) => {
    //     const value = event.target.value;
    //     setEnds(value);
    // }

    // const handleDescriptionChange = (event) => {
    //     const value = event.target.value;
    //     setDescription(value);
    // }

    // const handleMaxPresentationsChange = (event) => {
    //     const value = event.target.value;
    //     setMaxPresentations(value);
    // }

    // const handleMaxAttendeesChange = (event) => {
    //     const value = event.target.value;
    //     setMaxAttendees(value);
    // }

    // const handleLocationChange = (event) => {
    //     const value = event.target.value;
    //     setLocation(value);
    // }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // const data = {};
        // data.name = name;
        // data.starts = starts;
        // data.ends = ends;
        // data.description = description;
        // data.max_presentations = maxPresentations;
        // data.max_attendees = maxAttendees;
        // data.location = location;

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name:"",
                starts:"",
                ends:"",
                description:"",
                max_presentations:0,
                max_attendees:0,
                location:"",
            })
        }
    }

        const handleInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value
            })
        }

    // LOADING LOCATION DATA FOR OUR LOCATION DROPDOWN
    const fetchData = async () => {
        // MAKE AN API CALL TO THE LIST OF LOCATIONS
        const locationUrl = 'http://localhost:8000/api/locations/';
        // FETCH THE DATA AT THE LIST STATES URL
        const locationResponse = await fetch(locationUrl);
        // IF RESPONSE IS OKAY
        if (locationResponse.ok) {
            // CHANGE THE RESPONSE TO A JSON FORMATTED DATA RESPONSE
            // ADD AWAIT SO THAT IT DOESN'T RETURN THE PROMISE BUT INSTEAD THE
            // VALUE OF THAT PROMISE WILL TURN INTO AFTER AWAIT
            const locationData = await locationResponse.json();
            setLocations(locationData.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <div className="my-5 container">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                {/* <!-- NAME PROPERTY --> */}
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleInput} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                {/* <!-- STARTS --> */}
                <div className="form-floating mb-3">
                    <input value={formData.starts} onChange={handleInput} placeholder="starts" required type="datetime-local" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                {/* <!-- ENDS --> */}
                <div className="form-floating mb-3">
                    <input value={formData.ends} onChange={handleInput} placeholder="ends" required type="datetime-local" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                {/* <!-- DESCRIPTION --> */}
                <div className="mb-3">
                    <textarea value={formData.description} onChange={handleInput} placeholder="Description..." required type="text" name="description" id="description" className="form-control" rows="3"></textarea>
                </div>
                {/* <!-- MAX PRESENTATIONS --> */}
                <div className="form-floating mb-3">
                    <input value={formData.max_presentations} onChange={handleInput} placeholder="max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                {/* <!-- MAX ATTENDEES --> */}
                <div value={formData.max_attendees} onChange={handleInput} className="form-floating mb-3">
                    <input placeholder="max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                {/* <!-- LOCATION NUMBER --> */}
                <div className="mb-3">
                    <select value={formData.location} onChange={handleInput} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map((location) => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    )
    }

export default ConferenceForm
