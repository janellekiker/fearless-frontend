import React, { useEffect, useState } from 'react'

const AttendConferenceForm = () => {
  const [conferences, setConferences] = useState([]);
  const [formData, setFormData] = useState({
    name:"",
    email:"",
    conference:"",
  })

  // const [name, setName] = useState("");
  // const [email, setEmail] = useState("");
  // const [conference, setConference] = useState("");

  // const handleNameChange = (event) => {
  //   const value = event.target.value;
  //   setName(value);
  // }

  // const handleEmailChange = (event) => {
  //   const value = event.target.value;
  //   setEmail(value);
  // }

  // const handleConferenceChange = (event) => {
  //   const value = event.target.value;
  //   setConference(value);
  // }

  const handleSubmit = async (event) => {
    event.preventDefault();

    // const data = {};
    // data.name = name;
    // data.email = email;
    // data.conference = conference;


    const attendeeUrl = `http://localhost:8001/api/attendees/`;
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
        setFormData({
          name:"",
          email:"",
          conference:"",
        })

        // setName("");
        // setEmail("");
        // setConference("");
    }
}
    const handleInput = (event) => {
      setFormData({
        ...formData,
        [event.target.name]: event.target.value
      })
    }


  // LOAD CONFERENCE DATA IN DROPDOWN
  const fetchData = async () => {
    // MAKE AN API CALL TO THE LIST OF CONFERENCES
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    // FETCH THE DATA AT THE LIST CONFERENCES URL
    const conferenceResponse = await fetch(conferenceUrl);
    // IF RESPONSE IS OKAY
    if (conferenceResponse.ok) {
      // CHANGE THE RESPONSE TO A JSON FORMATTED DATA RESPONSE
      // ADD AWAIT SO THAT IT DOESN'T RETURN THE PROMISE BUT INSTEAD THE
      // VALUE OF THAT PROMISE WILL TURN INTO AFTER AWAIT
      const conferenceData = await conferenceResponse.json();
      setConferences(conferenceData.conferences)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (conferences.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  return (
    <div>
      <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select value={formData.conference} onChange={handleInput} name="conference" id="conference" className={dropdownClasses} required>
                    <option value="">Choose a conference</option>
                    {conferences.map((conference) => {
                                return (
                                    <option key={conference.href} value={conference.href}>
                                        {conference.name}
                                    </option>
                                );
                            })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input value={formData.name} onChange={handleInput} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input value={formData.email} onChange={handleInput} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  )
}

export default AttendConferenceForm
