import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Nav from './Nav';
import AttendeesList from './components/attendees/AttendeesList';
import LocationForm from './components/locations/LocationForm';
import ConferenceForm from './components/conferences/ConferenceForm';
import AttendConferenceForm from './components/attendees/AttendConferenceForm';
import PresentationForm from './components/presentations/PresentationForm';
import MainPage from './MainPage'

function App(props) {
  if (props.attendees === undefined) {
    return null;
    }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        {/* ATTENDEES PATHS */}
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />}></Route>
          <Route path="new" element={<AttendConferenceForm />}></Route>
        </Route>
        {/* CONFERENCE PATHS */}
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />}></Route>
        </Route>
        {/* LOCATION PATHS */}
        <Route path="locations">
          <Route path="new" element={<LocationForm />}></Route>
        </Route>
        {/* PRESENTATION PATHS */}
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />}></Route>
        </Route>
        </Routes>
    </BrowserRouter>
    </>
  );
}


export default App;
